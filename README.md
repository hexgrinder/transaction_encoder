### Description ###

This program provides a CLI for assigning account transactions with their equivalent BAI2 transaction code.

The account transactions are appended with their transaction code. The result is rendered to an output file.