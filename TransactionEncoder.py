"""
TransactionEncoder.py

Generates transaction codes.
"""
import re

class TransactionEncoder(object):

    def __init__(self, config):
        assert type(config) is dict, "'config' must be a dict type."
        assert type(config['transaction_codes']) is list, "'transaction_codes' must be a list type."
        self.__transaction_codes__ = config['transaction_codes']
    
    def generate_code(self, text):
        """
        Performs a text match then returns the matching
        transaction code.
                     
        :param text: A text string to be evaluated.
        :return: A code associated with the passed in text.
        """
        for codes in self.__transaction_codes__:
            for phrase in codes['phrases']:
                if (None != re.search(phrase, text, re.I)):
                    return codes['code']
        return None

class Sentence_TransactionEncoder(TransactionEncoder):

    def generate_code(self, text):
        """
        Performs a text match, then returns a list of matching transaction
        codes.

        :param text: A text string to be searched.
        :return: Returns a list of codes associated with the text. If there
        are no matches, the method will return an empty list.
        """
        valid_codes = []
        # assumption is that each word is located in the 1st index position
        words = [x[0] for x in map(lambda w: re.findall('[aA-zZ]+', w), re.split('\W+', text)) if len(x) > 0]
        for word in words:
            code = super(Sentence_TransactionEncoder, self).generate_code(word)
            if (None == code):
                continue
            valid_codes.append(code)
        return valid_codes
