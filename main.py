import json, os

from datetime import datetime
from TransactionEncoder import TransactionEncoder
from TransactionIO import Citi_TransactionReader, Citi_TransactionWriter

def _main_(csv_filename, config_filename, result_filepath):

    print "Processing file '%s'\n" % csv_filename
    
    timestamp = datetime.now().strftime("%Y-%m-%d_%H_%M_%S")

    coded_name = r'coded_{0}.csv'.format(timestamp)
    coded_filename = os.path.join(result_filepath,coded_name)
    
    uncoded_name = r'uncoded_{0}.csv'.format(timestamp)
    uncoded_filename = os.path.join(result_filepath, uncoded_name)
        
    # todo: get this from config file
    transaction_desc_index = 2

    with open(config_filename, 'rb') as config_file:
        config = json.load(config_file)
        encoder = TransactionEncoder(config)
    
    coded_cnt = 0
    uncoded_cnt = 0
    
    with open(csv_filename, 'rb') as csvfile:

        reader = Citi_TransactionReader(csvfile)
        
        with open(uncoded_filename, 'wb') as uncode_file:
            
            with open(coded_filename, 'wb') as coded_file:

                coded_writer = Citi_TransactionWriter(coded_file)
                uncoded_writer = Citi_TransactionWriter(uncode_file)

                for line in reader:

                    # if line already has a code, write to coded file
                    if (len(line) >= 5):
                        coded_writer.writerow(line)
                        coded_cnt += 1
                        continue

                    code = encoder.generate_code(line[transaction_desc_index])

                    if (None == code):
                        # no matches = human eyes
                        uncoded_writer.writerow(line)
                        uncoded_cnt += 1
                    else:
                        # ready for upload
                        line.append(code)
                        coded_writer.writerow(line)
                        coded_cnt += 1
        
        # tally results
        print '*** Transaction coding completed! ***\n'
        print '%d coded transactions.' % coded_cnt
        print '%d uncoded transactions.\n' % uncoded_cnt
        print "Resulting files saved in: '%s'\n" % result_filepath
        
if '__main__' == __name__:

    import sys
    
    try:
        if (1 >= len(sys.argv)):
            raise IOError('[Error] A file path was not specified.')
        
        __DEFAULT_CONFIG__ = r'transaction_citi_141.config'
        __DEFAULT_RESULTS_FILEPATH__ = os.path.join(
            os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0]))),
            'results')
        
        print '*** Hello! Welcome to Citibank transaction coder. ***\n'
            
        _main_(
            sys.argv[1], 
            __DEFAULT_CONFIG__,
            __DEFAULT_RESULTS_FILEPATH__)
 
    except IOError as ex:
        print ex
    except Exception as ex:
        print ex
    finally:
        print "Coding program exit.  Press 'Enter' to exit."
        raw_input()