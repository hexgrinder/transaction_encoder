import csv

class Citi_TransactionReader(object):

    def __init__(self, f, dialect=csv.excel, **kwds):
        self.reader = csv.reader(f, dialect=dialect, **kwds)

    def __iter__(self):
        return self
    
    def next(self):
        rtn = None
        row = self.reader.next()
        # acts as a spliter 
        for s in csv.reader(row): rtn = s
        return rtn

class Citi_TransactionWriter(object):

    def __init__(self, f, dialect=csv.excel, **kwds):
        self.__writer__ = csv.writer(f, dialect=dialect, **kwds)
        
    def writerow(self, row):
        # creates a single string that is surrounded by double-quotes
        self.__writer__.writerow([','.join(row)])

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)
